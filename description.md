Построение ряда отчетов на основе данных, хранящихся в HDFS:

1) Создаем внешние таблицы по исходным данным: логи пользователей, данные ip адресов, данные пользователей и подсети. 
Таблица логов партицированная по датам.
Сериализация и десериализация данных с использованием регулярных выражений.

2) Отчеты:
- количество посещений по дням
- количество посещений от мужчин и от женщин по регионам
- обновим базу логов (переезд сайтов в домен .com.), используем стриминг

3) Визуализация данных:
Обработка данных на лету посредством семплирования (выборка посещений от М и Ж по регионам). 
Сравнение точности результатов в зависимости от процента семплов. 
Визуализация на графике Питоном. 
