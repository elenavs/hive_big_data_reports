-- работаем в hue --

-- 1) Cоздаем внешние таблицы по исходным данным: логи пользователей, данные ip адресов, данные пользователей и подсети. --
-- Таблица логов партицированная по датам. Сериализация и десериализация данных с использованием регулярных выражений. --

--IPRegions--

USE suharevael;

DROP TABLE IF EXISTS IPRegions;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Subnets;

CREATE EXTERNAL TABLE IPRegions (
    ip STRING,
    region STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/user_logs/ip_data_M';

--Users--

CREATE EXTERNAL TABLE Users (
    ip STRING,
    browser STRING,
    gender STRING,
    age INT
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_data_M';

--Subnets--

CREATE EXTERNAL TABLE Subnets (
    ip STRING,
    mask STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/subnets/variant1';

SELECT * FROM IPRegions LIMIT 10;
SELECT * FROM Users LIMIT 10;
SELECT * FROM Subnets LIMIT 10;

--Logs (партиционированная по датам)--

ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;

set hive.exec.max.dynamic.partitions=500;
set hive.exec.max.dynamic.partitions.pernode=500;

USE suharevael;

DROP TABLE IF EXISTS Logs_v1;

CREATE EXTERNAL TABLE Logs_v1 (
    ip STRING,
    req_date INT,
    query STRING,
    page_s INT,
    http_s INT,
    u_info STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
        "input.regex" = '^\\s*((?:\\d{1,3}\\.){3}\\d{1,3})\\s+(\\d{8})\\d+\\s+(\\S+)\\s+(\\d+)\\s+(\\d+)\\s+(\\S+).*$'
)
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_logs_M';

---------------------------

SET hive.exec.dynamic.partition.mode=nonstrict;

USE suharevael;

DROP TABLE IF EXISTS Logs;

CREATE EXTERNAL TABLE Logs (
    ip STRING,
    query STRING,
    page_s INT,
    http_s INT,
    u_info STRING
)
PARTITIONED BY (req_date INT)
STORED AS TEXTFILE;

INSERT OVERWRITE TABLE Logs PARTITION (req_date)
SELECT
    ip,
    query,
    page_s,
    http_s,
    u_info,
    req_date
FROM Logs_v1;

SELECT * FROM Logs LIMIT 10;
