-- количество посещений по дням --

USE suharevael;

SELECT req_date, count (1) as qnt
FROM logs
GROUP BY req_date
ORDER BY qnt DESC;

-- количество посещений от мужчин и от женщин по регионам --

USE suharevael;

SET mapreduce.job.name=hv; 
SET hive.auto.convert.join=false;

select region,
sum(IF(gender='male', 1, 0)) AS men,
sum(IF(gender='female', 1, 0)) AS women 
from ipregions join users on ipregions.ip = users.ip 
group by region;

-- обновим базу логов (переезд сайтов в домен .com.), используем стриминг --

USE suharevael;

SELECT TRANSFORM(ip, req_date, query, page_s, http_s, u_info)
USING "sed 's|.ru/|.com/|g'" AS ip, req_date, query, page_s, http_s, u_info
FROM Logs
LIMIT 10;
