#!/usr/bin/env bash

hive -f ext_tables_partition.sql

hive -f reports_building.sql

hive -f sampling_visualization.sql | ./sampl_visual_makeplot.py
