#! /usr/bin/env python3

import matplotlib.pyplot as plt
import sys


# coordinates x
x = [round(float(1 / 10), 2), round(float(1 / 9), 2), round(float(1 / 8), 2), round(float(1 / 7), 2),
     round(float(1 / 6), 2), round(float(1 / 5), 2)]

# coordinates y
#male = [5486, 6784.25, 7833.5, 8674, 9446.25, 11125]
#female = [2545.5, 2696.75, 3248.75, 3634.25, 3865.5, 4916.25]

for i in sys.stdin:
    y = i.strip().split()
    male.append(y[0])
    female.append(y[1])

fig, ax = plt.subplots(figsize=(8, 5), layout='constrained')
ax.set_xlabel('Bucket')
ax.set_ylabel('Logs')
ax.set_title('Region Average Logs')
ax.plot(x, male, color='blue', label='Male')
ax.plot(x, female, color='red', label='Female')
ax.grid()
ax.legend()
plt.savefig('Hive5.png')
#plt.show()
