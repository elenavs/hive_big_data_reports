-- Обработка данных на лету посредством семплирования (выборка посещений от М и Ж по регионам). --
-- Сравнение точности результатов в заисимости от процента семплов. --
-- Визуализация на графике. --

USE suharevael;
SET hive.exec.max.dynamic.partitions=500;
SET hive.exec.max.dynamic.partitions.pernode=500;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.auto.convert.join=false;

       
SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 10 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;

SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 9 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;

SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 8 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;

SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 7 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;

SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 6 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;

SELECT  ROUND(AVG(t.male),2) as avg_male,
        ROUND(AVG(t.female),2) as avg_female
FROM  (
        SELECT i.region,
        SUM(IF(u.gender='male', 1,0)) as male,
        SUM(IF(u.gender='female', 1,0)) as female
        FROM users TABLESAMPLE(BUCKET 1 OUT OF 5 ON rand()) AS u
        LEFT JOIN ipregions AS i on u.ip=i.ip
        WHERE i.region in ('Moscow', 'Moscow Oblast', 'Saint Petersburg', 'Leningrad Oblast') 
        GROUP BY i.region) t;
